let mix = require('laravel-mix');
mix.pug = require('laravel-mix-pug');

mix.js('src/js/app.js', 'js')
    .sass('src/sass/app.scss', 'css')
    .pug('src/pug/*.pug', '../../dist', {
        pug: {
            pretty: true
        }
    })
    .extract(['jquery'])
    .version()
    .sourceMaps()
    .setPublicPath('dist')
    .browserSync('your-domain.test');